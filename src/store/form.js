/* eslint-disable no-unused-vars */
import create from 'zustand';

export const useFormStore = create((set, get) => ({
  formData: [
    {
      type: 'input',
      label: 'Name',
      placeholder: 'Please enter your name',
      value: '',
      errorMessage: 'Please fill the name',
    },
    {
      type: 'select',
      label: 'Gender',
      placeholder: 'Please select the gender',
      value: {},
      errorMessage: 'Please fill the gender',
      options: [
        {
          label: 'Male',
          value: 'Male',
        },
        {
          label: 'Female',
          value: 'Female',
        },
      ],
    },
    {
      type: 'date',
      label: 'DOB',
      errorMessage: 'Please fill the DOB',
      placeholder: 'Please select the DOB',
      value: null,
    },
  ],
  editJson: null,
  handleFormSubmit: (response) => {
    const { formData } = get();
    set({
      formData,
      editJson: JSON.stringify(formData, null, 2),
    });

    alert(JSON.stringify(response, null, 2));
  },
  handleJsonSave: () => {
    const { editJson } = get();
    set({
      formData: JSON.parse(editJson),
    });
  },
  handleChange: (editJson) => {
    set({
      editJson,
    });
  },
  handleUpdate: () => {
    const { formData } = get();
    set({
      editJson: JSON.stringify(formData, null, 2),
    });
  },
}));
