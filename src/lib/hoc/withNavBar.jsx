/* eslint-disable react/display-name */
import { Box } from '@mui/material';

import { TopNavBar } from '@components';

export const withNavBar = (Component) =>
  function (props) {
    const { children, ...rest } = props;
    return (
      <Box sx={{ flexGrow: 1, height: '100%' }}>
        <TopNavBar />
        <Box
          sx={{
            width: '100%',
            height: 'calc(100% - 64px)',
            overflow: 'auto',
          }}
        >
          <Component {...rest}>{children}</Component>
        </Box>
      </Box>
    );
  };
