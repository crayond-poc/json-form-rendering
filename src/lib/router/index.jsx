import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import { HomeWithNavBar, ErrorBoundary } from '@pages';

const router = createBrowserRouter([
  {
    path: '/',
    errorElement: <ErrorBoundary />,
    children: [
      {
        index: true,
        element: <HomeWithNavBar />,
      },
    ],
  },
]);

function RouterApp() {
  return <RouterProvider router={router} />;
}

export default RouterApp;
