import { RenderComponent } from '@components';
import { withNavBar } from '@lib/hoc';
import { Box, Button, Grid } from '@mui/material';
import { useFormStore } from '@store';
import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Input } from '@atoms/input';
import { home_style } from './style';

function Home() {
  const {
    formData,
    handleFormSubmit,
    handleJsonSave,
    handleChange,
    editJson,
    handleUpdate,
  } = useFormStore((state) => ({
    formData: state.formData,
    handleFormSubmit: state.handleFormSubmit,
    handleJsonSave: state.handleJsonSave,
    editJson: state.editJson,
    handleChange: state.handleChange,
    handleUpdate: state.handleUpdate,
  }));

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm({ ...formData });

  React.useEffect(() => {
    handleUpdate();
  }, []);

  return (
    <Box sx={home_style.rootSx}>
      <Grid container spacing={4}>
        <Grid item xs={12} md={6} lg={6}>
          <form
            style={home_style.inputWrapperStyle}
            onSubmit={handleSubmit((data) => {
              handleFormSubmit(data);
              reset({ ...formData });
            })}
          >
            {formData?.map((value) => (
              <Box key={value?.label} sx={home_style.componentSx}>
                <Controller
                  control={control}
                  name={value?.label}
                  rules={{ required: value?.errorMessage ?? '' }}
                  render={({ field }) => (
                    <RenderComponent
                      {...value}
                      {...field}
                      componentType={value?.type}
                      fullWidth
                      errors={errors}
                    />
                  )}
                />
              </Box>
            ))}

            <Button
              sx={home_style.submitButtonSx}
              variant="contained"
              type="submit"
            >
              Submit
            </Button>
          </form>
        </Grid>

        <Grid item xs={12} md={6} lg={6}>
          <Input
            placeholder="Please enter your form json"
            multiline
            fullWidth
            rows={20}
            sx={{
              bgcolor: 'grey.50',
              '.MuiInputBase-input': { fontSize: '14px' },
            }}
            onChange={(e) => handleChange(e.target.value)}
            value={editJson}
          />

          <Button
            sx={home_style.submitButtonSx}
            variant="contained"
            onClick={() => handleJsonSave()}
          >
            Save
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
}

export const HomeWithNavBar = withNavBar(Home);
