export const home_style = {
  rootSx: {
    p: 4,
  },
  inputWrapperStyle: {
    display: 'grid',
    padding: '32px',
    marginTop: '16px',
    gap: '12px',
  },
  componentSx: {
    margin: 1,
  },
  submitButtonSx: {
    p: 1,
    fontSize: 14,
    mt: 1,
  },
};
