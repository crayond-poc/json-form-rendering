import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

export function DatePicker(props) {
  const { fullWidth, value, errors, ...rest } = props;
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DesktopDatePicker
        renderInput={(params) => (
          <TextField
            {...params}
            fullWidth={fullWidth}
            error={!!errors?.DOB?.message}
            helperText={errors?.DOB?.message}
            size="small"
          />
        )}
        {...rest}
        value={value ?? null}
      />
    </LocalizationProvider>
  );
}
