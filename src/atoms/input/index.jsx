import TextField from '@mui/material/TextField';

export function Input(props) {
  const { errors, value, ...rest } = props;
  return (
    <TextField
      error={!!errors?.Name?.message}
      helperText={errors?.Name?.message}
      value={value ?? ''}
      size="small"
      {...rest}
    />
  );
}
