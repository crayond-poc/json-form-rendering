import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

export function Select(props) {
  const { label, errors, value, placeholder, onChange, ...rest } = props;
  return (
    <Autocomplete
      onChange={(event, newValue) => {
        onChange(newValue);
      }}
      size="small"
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          placeholder={placeholder}
          error={!!errors?.Gender?.message}
          helperText={errors?.Gender?.message}
        />
      )}
      {...rest}
      value={value?.value ?? null}
    />
  );
}
