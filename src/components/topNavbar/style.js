export const topNavBar_style = {
  rootSx: {
    flexGrow: 1,
  },
  titleStyle: {
    fontSize: 16,
    textDecoration: 'none',
    color: '#fff',
    fontWeight: 600,
    marginRight: 20,
    textTransform: 'uppercase',
  },
  linkStyle: {
    fontSize: 14,
    textDecoration: 'none',
    color: '#fff',
    marginRight: 20,
    textTransform: 'uppercase',
  },
};
