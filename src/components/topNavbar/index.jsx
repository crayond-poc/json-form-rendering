import { AppBar, Box, Toolbar, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

import { topNavBar_style } from './style';

export function TopNavBar() {
  return (
    <Box sx={topNavBar_style.grow}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Link to="/" style={topNavBar_style.titleStyle}>
              Json Form Renderer
            </Link>
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
