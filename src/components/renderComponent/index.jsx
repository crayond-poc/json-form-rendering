import { DatePicker } from '@atoms/datePicker';
import { Input } from '@atoms/input';
import { Select } from '@atoms/select';

export function RenderComponent(props) {
  const { componentType } = props;

  if (componentType === 'input') {
    return <Input {...props} />;
  }
  if (componentType === 'select') {
    return <Select {...props} />;
  }
  if (componentType === 'date') {
    return <DatePicker {...props} />;
  }
}
