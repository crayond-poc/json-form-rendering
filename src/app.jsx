import './app.css';
import '@fontsource/poppins/400.css';
import '@fontsource/poppins/500.css';
import '@fontsource/poppins/600.css';
import '@fontsource/poppins/700.css';

import RouterApp from '@lib/router';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { createTheme } from '@mui/material/styles';

import { theme } from '@utils/theme';

const muiTheme = createTheme({
  ...theme,
});

function App() {
  return (
    <ThemeProvider theme={muiTheme}>
      <CssBaseline />
      <RouterApp />
    </ThemeProvider>
  );
}

export default App;
